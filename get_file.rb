#
# Description: <Method description here>
#

require 'azure/armrest'
require 'csv'

storage_account_name  = 'cfmesa'
resource_group        = 'cfmerg'
share_name            = 'testshare'
filename              = 'approver_projects_map.csv'
@category_name        = 'custom_tag_category'
@category_description = 'Custom Tag Category'

ems = $evm.root.attributes['ext_management_system']

conf = Azure::Armrest::Configuration.new(
  :client_id       => ems.authentication_userid,
  :client_key      => ems.authentication_password,
  :tenant_id       => ems.uid_ems,
  :subscription_id => ems.subscription
)

azure = Azure::Armrest::StorageAccountService.new(conf)

begin
  storage_account = azure.get(storage_account_name, resource_group)
  $evm.log(:info,'Getting Storage Account was successful')
rescue Azure::Armrest::NotFoundException => e
  puts("Couldn't find Storage Account or Resource Group: #{e}")
  $evm.log(:error,"Couldn't find Storage Account or Resource Group: #{e}")
  exit MIQ_ERROR
end

key = azure.list_account_keys(storage_account.name, resource_group).fetch('key1')

file = nil
begin
  file = storage_account.file_content(share_name, filename, key)
  $evm.log(:info,"Grabbing the file handle was successful")
rescue Azure::Armrest::NotFoundException => e
  puts("Couldn't find file: #{e}")
  evm.log(:error,"Couldn't find file: #{e}")
  exit MIQ_ERROR
rescue Azure::Armrest::BadRequestException => e
  puts("Couldn't find share name: #{e}")
  evm.log(:error,"Couldn't find share name: #{e}")
  exit MIQ_ERROR
end

@csv_content = CSV.parse(file.to_s, :headers => true)

# List all the project tags from the CSV file, and put them into an Array
def get_project_tags()
  list_of_tags = []
  @csv_content.each do |line|
    list_of_tags.push(line[4])
  end

  list_of_tags.uniq
end

# Define all the project tags in MS Azure, by grabbing the tags from CSV
def populate_project_tags()

  unless $evm.execute('category_exists?', @category_name)
    $evm.execute('category_create', :name => @category_name,
    :single_value => true,
    :description => @category_description)
    $evm.log(:info,"Adding tag category: #{@category_name}")
  end

  get_project_tags().each do |record|
    # The tag name needs to be lowercase, otherwise the function will error out
    tag_name = record.downcase

    # TODO: Maybe have another column, with a more understandable description. So
    #       that we can understand what the project code is for
    tag_display_name = record.force_encoding(Encoding::UTF_8)

    unless $evm.execute('tag_exists?', @category_name, tag_name)
      $evm.execute('tag_create', @category_name, :name => tag_name,
                   :description => tag_display_name)
      $evm.log(:info,"Adding tag '#{tag_name}' to category '#{@category_name}'")
    end
  end
end

populate_project_tags()
exit MIQ_OK
